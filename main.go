// Distribution of this source code is subject to the MIT license (See LICENSE)
package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func processResponse(resp string, suffixA string) string {
	for _, line := range strings.Split(resp, "\n") {
		l := strings.Split(line, ":")
		if strings.ToUpper(suffixA) == strings.ToUpper(l[0]) {
			return l[1]
		}
  }
	return "0"
}

func callApi(prefix string) (string, error) {
	resp, err := http.Get("https://api.pwnedpasswords.com/range/" + prefix)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func hash(pw string) string {
	h := sha1.New()
	io.WriteString(h, pw)
	hash := h.Sum(nil)
	return hex.EncodeToString(hash)
}

func prefix(hash string) string {
	return string(hash[:5])
}

func suffix(hash string) string {
	return string(hash[5:])
}

func main() {
	args := os.Args
	if len(args) < 2 {
		fmt.Println("Enter password to check.")
		return
	}
	hsh := hash(args[1])
	pfx := prefix(hsh)
	sfx := suffix(hsh)
	body, err := callApi(pfx)
	if err != nil {
		fmt.Println(err)
		return
	}
	result := processResponse(body, sfx)
	fmt.Printf("\n\n%s\n\n\t", "Your password occurred")
	fmt.Println(result)
	fmt.Printf("\n%s\n\n", "times in the pwned passwords data base.")
}
